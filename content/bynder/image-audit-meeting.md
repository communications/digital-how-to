---
title: Image Audit meeting
date: 2018-07-19
---

## Introduction:

A meeting to discuss the next steps for the selection of images in the server, and how should Bynder be administered in terms of content.

#### Present:

- Cian
- Edward
- Filipe
- Spilios
- Tabea

## Status of images in OIPA server

- No photos under 1000 px wide, except some on Science
- 5152 images - 80 GB
- Broad categorization as:
  - Events
    - Presentation
  - Locations
    - ATC
  - People
  - Portraits
  - Science
  - Various
    - Food
    - Sports
- Unclear Copyright decision
  - CC BY NC SA according to Dan, but specifics will come in August from Legal
- Tags: singular, lowercase, surname, abbreviations, no special characters
- Filenames unchanged

## Objectives

- Define categories

  - delete photographs category on Bynder (because that's 99%)
  - kill food folder
  - Portrait as tags - specific

- Decide on photo variations: different angle, exposure, etc

  - Tabea will select photos and create guidelines

- Decide on what is not useful as an image

  - Diamond approach to Bynder, the cream of the crop

- Decide if to keep photos in server and Bynder, or just Bynder, or photos in server and a selection in Bynder

  - Bynder - source of truth, no need for a true mirror - Bynder is the equivalent of a searchable "Common pics and graphics" folder

- Set rules for file naming

  - yymm_keyword_person - to be discussed in more detail and be specific to categories (when_what_who_where)

- Set minimum technical requirements

  - one rule for all
  - 1000 px width minimum, but ask for the biggest they have

  

### Ask Tabea (on Monday)

- Ask about people's portraits in terms of privacy law
  - In August to discuss more details during a meeting with Legal and Photolab
- Ask about CMYK only photos, should these be converted to RGB?
  - No, goes as CMYK with RGB added as a manual derivative.
- Photos from external people (legal concerns)
  - Dan's email explains it



## Tasks (not implemented)

Game:

- Everyone gets a photo from different folders and uploads them to Bynder with proper metadata
  - Comparing different ways to add descriptive metadata

