+++
title = "Git and the terminal"
menuTitle = "Git"
date =  2019-06-13T14:30:00+02:00
+++

Creating content or making changes to the site in the terminal can feel daunting, but the amount of command line usage is actually quite small and simple to follow. You'll also gain far more control on the changes you make to the content or the site itself, along with many other benefits.

### Git

Make sure that git is installed in your system:

- Open a terminal. type `git` and execute;
- If there's an error, git is probably not installed. Find online the instructions to install it.

### Getting a clone of this repo

To have a copy of this repository on your local disk, go to or create a folder in your disk where you would like the repository to be copied to. Open a terminal on that location and execute the following command:

`git clone https://git.embl.de/grp-stratcom/digital-how-to.git`

{{%notice note %}}
This git location is found in the [Gitlab repository](https://git.embl.de/grp-stratcom/digital-how-to/) when pressing the *Clone* button (top right). This is the *Clone with HTTPS* option.
{{%/notice %}}

The terminal should present some information about files being copied, deltas, etc.

### Pushing and pulling

- A *pull* brings updates from the remote repo to the work folder
- A *push* sends updates from the work folder to the remote location

{{%notice tip %}}
As a good practice, and to avoid very annoying problems with merges, always start by making a pull request before working on the folder.
{{%/notice %}}

### Pushing changes you've made

So you've made a change to one of these documents and you want to push the changes to change the website. This is the basic procedure on the terminal:

- `git status` - to see what you changed
- `git add file1 file2` - name the files to acknowledge the changes made
- `git commit -m "message"` - change message to a meaningful description of what changed and add the files to the staging area
- `git status` - check if they're now in the staging area and all is good
- `git push` - push the changes to the remote location

You can check the results of your push in the Gitlab repository side menu under *CI/CD*. If you have a failed job, take a look at the log or just ask Ken for help.

And on everything else ---> Ken

