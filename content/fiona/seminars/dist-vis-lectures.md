+++
title = "EMBL Distinguished Visitor Lectures"
menuTitle = "Distinguished Visitor Lectures"
date =  2019-06-18T10:30:00+02:00
+++

[EMBL Distinguished Visitor Lectures](https://www.embl.de/research/seminars/distinguished-visitor-lectures/index.html)

[EMBL Distinguished Visitor Lectures folder in Fiona](https://nps2.embl.de:8443/default/NPS/p/b_41d/cb*/EMBL/content/research/03-seminars/common/seminars/distinguished-visitor-lectures*#x)

### Creating a speaker page

1. Inside the appropriate year folder, use *Edit > Copy tree* of an existing folder and paste it in year folder.

	{{% notice tip %}}
This procedure copies the folder and any files within the folder. Careful on how deep it goes, or if there are large files involved.
{{%/ notice %}}

2. Edit the image inside the lecture folder:
   - *File > Import file* and replace the photo (155x175px);
   - Rename the file to the format *year-month-day_surname*;
   - Change the title to the speaker's name;
   - Change the *text-column* with the provided information about the talk and speaker.

3. In the lecture folder:
   - Change the name to the format *month-day-surname*;

4. In the year folder:
   - Edit the *Main content*, copy-paste one lecture, change accordingly.

5. In the distinguished visitor lectures folder:
   - Copy-paste one the lectures images;
   - Rename it as *year-month-day_surname*;
   - Change the title to the speaker's name;
   - Set the *Order* based on the lecture's date;
   - Change the *text-column* with the provided information about the talk and speaker;
   - Add a link to the speaker's page created earlier, with the name in the *Title*;
   - Set a *Valid until* date to after the lecture is finished.

{{% notice note %}}
If the abstract or biography is missing, write *To be added* in italics (mind the talk's language) until you're provided with one.
{{%/ notice %}}

6. If the previews are good, release all of the content (folders and images).

### Editing the seminar database

[Seminar Database](https://www-db.embl.de/jss/seminarlist.html?administer=x&administer=x)

- Make sure to be on the correct EMBL location.
- Find the intended talk and click *edit* (right side) to change the contents.
- Change accordingly and confirm that the event shows the information correctly.

{{% notice note %}}
If the abstract or biography is missing, **don't** write *To be added*, just leave it blank. Make sure to update this section when you're provided with one.
{{%/ notice %}}

{{% notice tip %}}
The seminars database updates all information immediately, and you can find the seminars on the EMBL homepage under the news.
{{%/ notice %}}

----

The EMBL Distinguished Visitor Lectures contact person is [Nina Bader](mailto:nina.bader@embl.de)

