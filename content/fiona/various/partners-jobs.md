+++
title = "Adding job offers from partner institutions"
menuTitle = "Partners jobs"
date =  2019-05-28T14:30:00+02:00
+++

EMBL promotes job offers from partner institutions in the [Related Opportunities page](https://www.embl.de/jobs/partners/index.html). Some names of partner institutions are:

- Nordic Partnership (DANDRITE, FIMM, NCMM, MIMS);
- EMBLEM;
- EMBO;
- HCEMM;
- and others...

## Adding a job offer from a partner institution

You will receive a ticket in Kayako from someone in these institutions with information about a job offer.

1. Access the related oportunities [Fiona folder](https://nps2.embl.de:8443/default/NPS/p/b_440/cb*/EMBL/content/hr/recruitment/partners*#x) and edit the relevant institution's document;

2. Copy-paste a job offer from the document and change accordingly:
   - Title
   - Deadline
   - Location

{{% notice info %}}
Normally we don't share pdf/doc job offers, since the information provided is usually present in the institution's job pages, and that's where the job application takes place.
{{% /notice %}}

3. In the title of the offer, add the link to the job application page. This is a page in the institution's website, so mark it to open in a new window;

4. Save the document and release it.

5. Once it's live, email Rachel Coulthard to update the intranet [vacancy mailing list](mailto:vacancy.distribution@embl.de) by copying the job offer from the website (link included).

{{% notice tip %}}
Periodically test the page for broken links and job offers whose deadline is over or has been extended.
{{% /notice %}}
