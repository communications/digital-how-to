+++
title = "Adding a press release"
menuTitle = "Press release"
date =  2018-09-11T20:28:09+02:00
weight = 10
+++

## Press release from EMBLetc

{{% notice warning %}}
Before following this guide, please upload the press release news to a [news folder in Fiona](https://nps2.embl.de:8443/default/NPS/p/b_5e/cb*/EMBL/content/aboutus/strategy-and-communications/strategy-and-communications/news*#x) according to [EMBL News guide](https://grp-stratcom.embl-community.io/digital-how-to/fiona/news/embl-de/)
{{% /notice %}} 

### Setting up the folder

1. Go to the [News folder in Fiona](https://nps2.embl.de:8443/default/NPS/p/b_5e/cb*/EMBL/content/aboutus/strategy-and-communications/strategy-and-communications/news*#x), select the folder with the intended news piece > *Edit* > *Copy tree*;

2. Go to the [Media Relations folder in Fiona](https://nps2.embl.de:8443/default/NPS/p/b_2c9/cb*/EMBL/content/aboutus/strategy-and-communications/strategy-and-communications/media_relations/) and paste the copied folder into the appropriate year folder;

3. Change the fields of the folder:

| Field | Value |
| ---- | ---- |
| *Valid from* | Today's date or future date if applicable |
| *Press contacts* | In *Destination* paste "/EMBL/content/aboutus/strategy-and-communications/pr_contact/pr_hd_contact" > *Add* > *Ok* |
| *External news link* | Delete the news link |
| *Channel* | Change "*embl_news*" to "*press releases*" (it will be displayed as *news*) |

### Adding the main content

1. Open *Main content*, paste the news piece and style as desired;
{{% notice tip %}}
The cover image to use here is the file in the current folder ending with **ib**
{{% /notice %}}

2. At the end of the article place the following sentence: `This post was originally published on EMBL news.`;

3. Select "EMBL news" and add an hyperlink to the news piece in EMBLetc;
{{% notice info %}}
Search engines follow all links in a page when indexing, and downgrade in the listing sites with repeated content. Because of this, we need to add an element to the link to prevent the indexing process to follow this link. 
{{% /notice %}} 

4. With the pointer close to the "EMBL news" words, click on the button that spells "HTML";

5. Copy the code `rel="canonical nofollow noopener"`, paste as portrayed in `<a -->code here<-- href="https://news.embl.de/...">`, and click *Update*;

6. As usual, use the preview functionality to see the changed page before releasing the page;
{{% notice tip %}}
You don't need to add your contact details as they were added previously in "Press contacts", and will be displayed automatically.
{{% /notice %}} 

7. If all is good, publish the item by selecting in the main menu *Edit > Subtree actions > Release > Release > Close*;

8. Remember to log out from the CMS once you're done.

## Press release with original content


## Press release with multiple languages

To add a flag icon:

1. Please download a respective icon from the server: [Flag icons](smb://oipa.embl.de/oipa/_DIGITAL/Web/Content/Press_releases/flag_icons)
2. Upload the icon as a standard image to the folder with a press release.
3. In the Fiona text editor add an image and choose your icon. 

Anchors:
{{% notice warning %}}
Make sure to test if the HTML anchors used to jump to the translated sections in the page work correctly. Anchors need an id as a destination point.
{{% /notice %}} 

