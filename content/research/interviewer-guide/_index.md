+++
title = "Interviewer guide"
date =  2018-11-14T08:42:23+01:00
+++

| Date | Time | Location |
| ---- | ---- | -------- |
| ---- | ---- | -------- |

#### 2. Introduction

My name is _________. I work in the Strategy and Communications department, and I would like to thank you for taking the time to participate in this project study.

We want to develop an intranet that is more user focused. Based on previous interviews and analytics, we are heading towards a task-centred approach, and with this study we want to find out if our assumptions are leading us in a good direction, a direction that makes it more useful to you.

With this in mind, I will be asking you 2 questions about your role in EMBL, some ideas on how you use the intranet, followed by a short card game. Finally, I would like you to see a prototype of the project and to share any thoughts or questions that you might have.

At any point, please ask any question you wish and comment on any matter. All information gathered here is going to be used solely for this study, and will be treated in anonymity.

Thank you again for taking the time to participate. Do you have any questions or comments before we begin?

#### 3. Questions about the participant

- In one or two sentences, what is your current role in EMBL?

- For how long have you been a part of EMBL?

#### 4. Questions about Intranet usage

- How often do you use the intranet, any type of use, on a weekly basis?

- Is the information presented on the intranet essential to your work?
	- If yes – In what way?

#### 5. Top 5 tasks

You can find on the table some cards of goals when using the intranet. What I would like to ask you to do, is to go through these cards and choose the most common 5 goals that you use the intranet for. It doesn’t matter if it’s directly work specific or not, any use for the intranet is relevant.

Afterwards, please put them in a sequence, from most frequent to less frequent.

If you think of a goal that is not on any of [these cards][../top-tasks/cards.md], please write it on one of the empty cards. It does not have to be on your Top 5 goals.

| 1 | 2 | 3 | 4 | 5 |
|---|---|---|---|---|
|---|---|---|---|---|

#### 6. Questions about the card game

- How often do you have these goals?

- Which ones of the top 5 are directly work related?

- Is any of the top 5 cards saved as a bookmark or if a document, downloaded on your computer?
	- Why did you bookmark/saved it?

- How often do you use these bookmarks/documents?

- What other bookmarks or documents from the intranet do you have?

#### 7. Questions about Springboard

I would like to show you a prototype of a project named Springboard. Based on past interviews and analytics, we are experimenting with a dashboard layer to the current intranet. It’s a selectable view, not a replacement for the intranet, an overlay.

- Were you expecting this approach?
	- Why?

- What kind of expectations do you have with a dashboard layer the intranet?

- What should be there?

- What else would you like to see?

#### 8. Consent for further participation

I would like to ask if you would be interested in continuing your participation in this project development. This would be done at your convenience. If you would like to participate further, please fill in your name and email address in the following form.

*Yes, I would like to participate further in the development of Springboard:
Name: _____________________________________
e-mail: ___________________________________*

#### 9. End

This marks the end of the session. Do you have any further questions or comments regarding this session, the project being developed, or any other subject?

I would like to thank you once again for participating in this study. Any question or comment that pops up, feel free to contact myself or my colleagues at - *give card* (stratcomm contacts).

If you want to know more about this project and other projects from Stratcomm, make sure to follow us on the blog link in the card or just come and meet us in the ATC building. A big thank you from all of us.

| Notes |
| ----- |
| ----- |

## PDF of the Interviewer guide

{{%attachments title="PDF of the Interviewer guide" pattern=".*pdf"/%}}
