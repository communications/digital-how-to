+++
title = "Research"
date = 2019-02-28T16:22:10+01:00
pre = "<b>4. </b>"
chapter = true
weight = 9
+++

# Research

## User research on the Intranet

{{% children style="h4" %}}

----

#### Research data

- OIPA server / People / Filipe / User Research
- Paper documentation inside the "UX box" in the Digital office
