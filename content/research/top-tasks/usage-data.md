+++
title = "Data by usage"
date =  2018-11-14T08:42:23+01:00
+++

### Usage of card choices
#### Question 6a - How often do you have these goals?
##### Data added from user 1 to user 10

#### Daily

- Contact of someone in a specific role
- Internal events list
- Group information
- IT how-to
- Links to tools
- Contact of a specific person
- Calendar
- Booking a room

#### A few times per week

- Booking a room
- Unit information
- HR information
- Service request (IT)
- Links to tools
- Staff association information
- Contact of a specific person
- Canteen menu
- Group information

#### Once a week

- Kindergarten information
- Internal news
- HR information
- Internal events list
- Purchasing information
- Training information
- SAP ticket
- Converis ticket
- Service request (IT)
- Booking a room
- Canteen menu
- Contact of a specific person

#### Every 2 weeks

- Policy document
- Mailing list information
- Contact of a specific person
- Intermedex
- PhD information
- Contact of someone in a specific role
- Catering order form
- Facility management information
- Template
- HR information
- Internal Events

#### Monthly

- Library catalogue
- Service request (IT)
- Contact of someone in a specific role
- Booking a room
- Intermedex
- CMS ticket
- Mailing list information
- Group information
- Catering order form
- Email vacation/out-of-office message

#### User data from Top Tasks

| Order | user 1 | user 2 | user 3 | user 4 | user 5 |
|:-----:|:------:|:------:|:------:|:------:|:------:|
| **1** | Contact of someone in a specific role | Internal events list | Group information | Contact of a specific person	 | Calendar |
| **2** | Unit information | Contact of a specific person | Contact of someone in a specific role | Group information | Contact of someone in a specific role |
| **3** | Links to tools | Booking a room | IT how-to | HR information | Training information |
| **4** | Booking a room | Library catalogue | Links to tools | Internal events list | Purchasing information |
| **5** | HR information | Contact of someone in a specific role | Kindergarten information | Policy document | Internal news |
| **6** | Staff association information | Service request (IT) | Internal news | Mailing list information | Intermedex |
| **7** | Service request (IT) |  | HR information | Booking a room |  |  |
| **8** |  |  |  | Canteen menu |  |

----

| Order | user 6 | user 7 | user 8 | user 9 | user 10 |
|:-----:|:------:|:------:|:------:|:------:|:-------:|
| **1** | Contact of a specific person | Booking a room | Booking a room | Contact of a specific person | Contact of a specific person |
| **2** | Service request (IT) | Canteen menu | Contact of a specific person | Booking a room | Group information |
| **3** | Converis ticket | Contact of a specific person | Catering order form | Facility management information | Booking a room |
| **4** | SAP ticket | Intermedex | Contact of someone in a specific role | Group information | HR information |
| **5** | CMS support | PhD information | Mailing list information | Catering order form | Template |
| **6** | | | | Email vacation/out-of-office message | Policy document |
| **7** | | | | | Internal Events |

