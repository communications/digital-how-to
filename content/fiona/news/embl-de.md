+++
title = "Adding news to embl.de"
menuTitle = "News"
date =  2018-08-20T14:28:09+02:00
weight = 6
+++

## Preparing the images
1. Save the image from the news piece.
2. Resize accordingly:

| Placement | Size (px) | Filename_xx |
| :------ | --------- | --------- |
| [TVs in HD Campus](http://infoboard.embl.de/) | 620x425 | _ib.jpg |
| [External homepage](https://www.embl.de/) | 427x427 | _ex.jpg |
| [Stratcom news list](https://www.embl.de/aboutus/communication_outreach/news/) | 175x120 | _ov.jpg |
| [Intranet homepage](https://intranet.embl.de/) | 120x76 | _in.jpg |

- Create a ZIP archive with the images.

## Setting up the news item in Fiona

1. Go to the [News folder in Fiona](https://nps2.embl.de:8443/default/NPS/p/b_5e/cb*/EMBL/content/aboutus/strategy-and-communications/strategy-and-communications/news*#x").

2. Make a copy of a news folder and paste it in the same directory.

3. Change the folder name according to the template **YYMMDD-keywords**.

4. Fill in the remaining fields of the folder:

| Field | Value |
| ---- | ---- |
| *Title* | Title of news article |
| *H2 - Subtitle* | Excerpt (max. 100 characters) - [Wordcounter](https://wordcounter.net/) |
| *Valid from* | Today's date or future date if applicable |
| *Location* | Barcelona - Grenoble - Hamburg - Heidelberg - Hinxton - Rome<br />*Note 1:* Not specific to a location: General<br />*Note 2:* Multiple locations: separate with a comma |
| *Abstract / Info for list view* | Abstract (max. 110 words) - [Wordcounter](https://wordcounter.net/)<br />Can be a copy-paste of the first paragraph or two of the news article, as long as the text makes sense in isolation.<br />*Note 1:* The text is also displayed in the EMBL website search and is indexed by Google as the 'meta-description' of the page (truncated after ca. 150 characters).<br />*Note 2:* Ensure that **individual**, or at least **group names**, are included in the abstract. |
| *Keyword list* | Keywords for discoverability, useful to check the ones used in the footer of the article |
| *External news link* | Copy-paste the URL to the article |
| *Channels* | **EMBL news** (always)<br/>Barcelona research: *Research Highlights Barcelona*<br/>Hamburg research: *Research Highlights Hamburg*<br/>Archive: *News Archive*<br/>Partnerships: *News from EMBL Partnerships*<br/>Tara: *Tara Tales/Stopovers* |

{{% notice info %}}
If you're uploading news a few days earlier - before releasing please make sure that the *Valid from* field reflects the day of publishing
{{% /notice %}} 

## Adding the images to the news item

1. While on the folder, go to the main menu and click *File > Import archive*;

2. Open the ZIP archive previously created;

3. For the *Destination*, select *Extract into this folder*. Then click *OK*;

4. When asked in the next screen which file formats to use, select according to the filename:
{{% notice warning %}}
Remember to de-select ‘Use this file format for all files of type ‘jpg’
{{% /notice %}}

   - **ib** - Infoboard image (620x425px)
   - **in** - Image for homepage (120x76px)
   - **ov** - Image for overview
   - **ex** - External block image

5. As usual, use the preview functionality to see the changed page before releasing the page;

6. If all is good, publish the item by selecting in the main menu *Edit > Subtree actions > Release > Release > Close*;

7. Remember to log out from the CMS once you're done.

{{% notice info %}}
When importing the images, Mac users will see a window requesting that you *Please select the file format which should be used to import the folder _MACOSX*: simply select OK > close subsequent error message > refresh the page in the CMS using the green arrows icon in the toolbar; the images should be visible.
{{% /notice %}} 
