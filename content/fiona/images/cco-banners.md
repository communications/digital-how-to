+++
title = "CCO banners"
date =  2018-08-31T18:29:24+02:00
weight = 3
+++

CCO banners are the sliders on the homepages of the public and intranet websites.

The images received are named with the event code (EES, CHR, etc) followed by the year, a sequential number, and the size of the image.
Each event has 2 images of different size and aspect ratio, being the *1280x440px* for the *public website*, and the *290x144px* for the *intranet website*.

Access the images and the *Excel file* with the listing of events and information, in this network drive location: `smb://photolabserver.embl.de/cco/Event%20Artwork`. Copy the needed banners and excel file to your local disk to avoid changing the original documents.

{{% notice note %}}
This procedure doesn't make use of mirrors, so everything has to be copied to all EMBL locations, except EBI.
{{% /notice %}}

### Intranet website

For the intranet website the small *290x144px* images will be used.

1. Go to the [rotation_2nd_column folder](https://nps2.embl.de:8443/default/NPS/p/b_383/cb*/EMBL/internal/HD/homepage/rotation_2nd_column*#x) in the EMBL Heidelberg intranet;

2. Copy one of the files in the folder and make how many copies as needed for the banner replacements;

3. Deactivate the banners to be replaced, but don't release them yet;

4. Using the Excel file provided, start updating the copies made on step 2:

   1. **Name** as *Code-title*, where title is a couple of keywords from the full title;
   2. **Title** as the full title of the event, removing the type of event (ex. EMBO Workshop:);
   3. **Link** is the hyperlink to the event page in Fiona CMS (/EMBL/content/cco/embl-heidelberg/events/2019/--event-code--), and add the full title again on the Link popup window, with Frame as *default*;
   4. Import the correct image with *File* > *Import File* as a replacement of the current version.

5. When all replacements are updated, select in the main menu *View* > *Table*;

6. Select all the new files and the recently deactivated files, and *Release* all of them;

7. Copy the new files to all the other EMBL locations folders (except EBI), and follow the same processes of deactivation and release.

### Public website

For the public website the wide *1280x440px* images will be used.

1. Go to the [sliders folder](https://nps2.embl.de:8443/default/NPS/p/b_383/cb*/EMBL/external/HD/homepage/sliders*#x) in the EMBL Heidelberg public website;

2. Same steps as 2 to 4 of the Intranet website, but with the following additions:

   1. **Order** has to be unique to avoid conflicts;
   2. **Color box** sets the color of the caption above the image. No rules here, just make a sensible choice;
   3. **Category to display** is in the *Full title* of the Excel file (ex. EMBO Workshop).

3. Follow the remainder steps of the Intranet website.

{{% notice tip %}}
The sliders can only be previewed once released, but there's no need to wait until they are live to see if all is correct. Just release them and click preview.
{{% /notice %}}

Once the release cycle of Fiona CMS is completed (about 1 hour) check on the intranet and public website of all locations if everything is working properly.

{{% notice note %}}
For any issues or questions, send an email to <a href="mailto:julie.heinecke@embl.de">Julie Heinecke</a> from CCO.
{{% /notice %}}
