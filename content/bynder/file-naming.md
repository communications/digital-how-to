---
title: "File-naming guidelines"
subtitle: "Guidelines for image file names uploaded to Bynder"
date: "2018-07-28"
---

## Guidelines for image file names uploaded to Bynder

- Use lowercase characters only
- Don’t use special characters e.g.:   \ / : * ! ? ". , < > | [ ] & $
- Use the underscore character (“_”) to separate filename chunks. Use the short dash (“-”) character to concatenate elements within a filename chunk
  - Example: 1807_ells-learning-labs_hd.jpg

- Use a meaningful name, having the following questions in mind: when, what, who, where (you may use these selectively according to the image category, file name length etc.)
  - Example (event):  1807_cpp-annual-meeting_hd.jpg
     - when_what_where
  - Example (portrait):  1807_fotis-kafatos.jpg
     - when_who
  - Example (science):   1807_cell-nucleus_korbel.jpg
     - when_what_who
  - Example (science - technology):   1807_ data-centre-blades_hx.jpg
     - when_what_where
  - Example (location):  1807_ prbb-aerial-view_bcn.jpg
     - when_what_where
- Use the date of creation of the file as a prefix in the format YYMM (no need to add the day)
  - Example:  1807_
- Use abbreviations instead of the full names
  - Example:  1807_cpp-annual-meeting_hd.jpg
     - cpp stands for Corporate Partnership Programme
- For multiple files with the same name (e.g. a collection of photos from an event taken on the same day) add a serial number at the end of the name in the following format: _01
  - Example:   1807_cpp-annual-meeting_hd_01.jpg, 1807_cpp-annual-meeting_hd_02.jpg
- Use a short title, try not to exceed 30 characters
