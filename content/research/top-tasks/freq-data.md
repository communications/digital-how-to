+++
title = "Data by frequency"
date =  2018-11-08T08:42:23+01:00
+++

### Total frequency of cards
#### Question 5
##### Data added from user 1 to user 10

- *Booking a room:* **7**
- *Contact of someone in a specific role:* **5**
- *Contact of a specific person:* **5**
- *HR information:* **4**
- *Group information:* **4**
- *Service request (IT):* **3**
- *Internal news:* **2**
- *Internal events list:* **2**
- *Links to tools:* **2**
- *Mailing list information:* **2**
- *Canteen menu:* **2**
- *Intermedex:* **2**
- *Policy document:* **2**
- *Catering order form:* **2**
- *Library catalogue:* **1**
- *IT how-to:* **1**
- *Kindergarten information:* **1**
- *Staff association information:* **1**
- *Unit information:* **1**
- *Calendar:* **1**
- *Purchasing information:* **1**
- *Training information:* **1**
- *Converis ticket:* **1**
- *SAP ticket:* **1**
- *CMS support:* **1**
- *PhD information:* **1**
- *Internal Events:* **1**
- *Email vacation/out-of-office message:* **1**
- *Template:* **1**
- *Facility management information:* **1**

#### User data from Top Tasks

| Order | user 1 | user 2 | user 3 | user 4 | user 5 |
|:-----:|:------:|:------:|:------:|:------:|:------:|
| **1** | Contact of someone in a specific role | Internal events list | Group information | Contact of a specific person	 | Calendar |
| **2** | Unit information | Contact of a specific person | Contact of someone in a specific role | Group information | Contact of someone in a specific role |
| **3** | Links to tools | Booking a room | IT how-to | HR information | Training information |
| **4** | Booking a room | Library catalogue | Links to tools | Internal events list | Purchasing information |
| **5** | HR information | Contact of someone in a specific role | Kindergarten information | Policy document | Internal news |
| **6** | Staff association information | Service request (IT) | Internal news | Mailing list information | Intermedex |
| **7** | Service request (IT) |  | HR information | Booking a room |  |  |
| **8** |  |  |  | Canteen menu |  |

----

| Order | user 6 | user 7 | user 8 | user 9 | user 10 |
|:-----:|:------:|:------:|:------:|:------:|:-------:|
| **1** | Contact of a specific person | Booking a room | Booking a room | Contact of a specific person | Contact of a specific person |
| **2** | Service request (IT) | Canteen menu | Contact of a specific person | Booking a room | Group information |
| **3** | Converis ticket | Contact of a specific person | Catering order form | Facility management information | Booking a room |
| **4** | SAP ticket | Intermedex | Contact of someone in a specific role | Group information | HR information |
| **5** | CMS support | PhD information | Mailing list information | Catering order form | Template |
| **6** | | | | Email vacation/out-of-office message | Policy document |
| **7** | | | | | Internal Events |

