+++
title = "Adding a Staff member"
menuTitle = "New staff member"
date =  2018-08-23T16:24:57+02:00
weight = 5
+++

Stratcom has two members pages (manually maintained), one on the [external website](https://www.embl.de/aboutus/communication_outreach/members_static_ext/index.html) and another on the [intranet](https://intranet.embl.de/communication_outreach/members_static_int/index.html). Each time a new member joins the team, or someone leaves, these pages have to be updated accordingly.

The procedure described below is almost the same for other static members pages on the website and intranet.

## Adding a new member to the intranet page

1. Map the portrait to the person in [ImageMapper](https://webda.embl.de/EMBLPersonGroup-ManageImages/) and save the photo;

2. Resize the photos to 115x130px;

3. Go to the [Intranet Stratcom members page](https://nps2.embl.de:8443/default/NPS/p/b_328/cb*/EMBL/content/aboutus/strategy-and-communications/strategy-and-communications/members_static_int*) in Fiona CMS;

4. Copy-paste an existing image file and update the filename and other text fields accordingly;

    {{% notice tip %}}
   To link to the new persons' individual page, you just need to replace the CPID code (CP-XXXXXXXX ) at the end of the current hyperlinked name. You can find the CPID code of the new member by visiting his or her individual page on the intranet (last part of the URL)
    {{% /notice %}}

5. Go to *File* > *Import image*, and replace the current image;

6. Release the image file when finished.

{{% notice note %}}
To remove a person, simply deactivate the correspondent image file, and re-release the file.
{{% /notice %}}

## Adding a new member to the external website

1. Go to the [External Stratcom members page](https://nps2.embl.de:8443/default/NPS/p/b_32f/cb*/EMBL/content/aboutus/strategy-and-communications/strategy-and-communications/members_static_ext/stratcom_ebi*) in Fiona CMS;

2. Add the name to the list under the appropriate team;

3. Change the link following the same procedure as described above;

4. Release the page when finished.

{{% notice note %}}
To remove a staff member, simply delete his or her entry from the list.
{{% /notice %}}

## No photo available yet?

{{%attachments title="Use these:" pattern=".*jpg"/%}}
