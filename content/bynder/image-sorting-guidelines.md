---
title: "Guideline to sort images in OIPA server"
date: 2018-07-25
---

In each of the folders Filipe created, we will have folders:

- _Archive
- _Bynder
- _Trash

#### What goes to Bynder:

- only the “diamond” images
- core content 
- benchmark and visually highly appealing images
- standard people portraits
- images, where rights and license are clear
- actual images of technologies

#### What goes to archive:

- image of heads of units, GL, directors
- historic images
- (old) instruments and technology
- event images, older than a year

#### What stays on the server 

- in general: more project based
- event images not older than a year
- group/team pictures, not older than a year
- any kind of documentation
- core content (like maps, logos, flags, site images, Heads of units and Directors portraits) will be in design folder
- Comms product, always the year/issue before the actual issue

### What goes to trash:

- unsharp images
- images, where we have several pictures with the same content, keep only one version
- images, where rights are not clear
- unappealing images
- images with irrelevant content

