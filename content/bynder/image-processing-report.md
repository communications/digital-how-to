---
title: Image processing report
subtitle: 
date: 2018-06-24
---

## Pathway to Bynder

### Metadata on files

Currently the focus is on editing photo metadata directly to the files using an image editor.

In this early phase with only one, hmmmm, tagger? (me), this is the most efficient method for organizing a large collection consistently and apply bulk actions when possible.

- Self-containment: changes made are present in the file and not on a separate file/database 
- Bulk changes: changes can be done in a standardized and bulk form, peticularly with copyright and tagging
- Speed and flexibility: no lag, it's local, and the choices of software allow for a more streamlined workflow

#### Why not use Bynder for this?

- Bynder writes to the XMP metadata on some fields, but is limited in options
- Much slower workflow than a local alternative
- The original file has to be downloaded to retain a copy in an EMBL server

### Metadata fields

- Title: max 10 words
- Caption: copy from press release caption used or more detailed title
  - I've also been adding the URL to the press release/news for further information and link to content created
- Credit: first and last name of the author of the photo
- Copyright: first and last name/EMBL. Unclear on non EMBL material?
- Rights/Usage/Terms: CC BY-NC-SA if EMBL. Unclear on non EMBL material?
- Tags: lower caps, singular, abbreviated when sensible, minimum of 4
- Filename: unchanged so far

## Bynder metaproperties

Using the Metaproperties Management panel, some have been added and experimented on during the past 2 weeks.

Currently the changes made were to show License (Terms), Credit, and Colour mode (CMYK/RGB). This information makes use of XMP metadata, and can be written to the hosted file when downloading by selecting the "include XMP metadata" option.

Asset Type, Location, and Units, are not XMP properties, they're custom options in Bynder, and as such are not retained when the file is downloaded. Perhaps consider how useful these options are?

### Issues

- Changes to metaproperties are not retroactive, and thus older uploads will not display these changes
- Copyright in Bynder is read first from EXIF, then XMP. Changes to EXIF are only possible outside of Bynder
- Colour mode only works for CMYK so far, but I guess that's not so much of a problem, other options exist
- Currently there is a bug in Bynder that doesn't allow an asset to change to a different Asset Type
- Still unsure on the full scope of information that can be retrieved by the search function
- Unclear on the easy management of tags and other metaproperties (merging, update current files, etc)

## Other Bynder points

- Versions is a potential useful feature, but more testing for usability/usefulness is needed
- Derivatives is not a useful feature so far and delays substantially the adding of assets to Bynder
  - Making changes to this feature is possible now as we don't have many images, but with a larger set this will not be free - according to the most recent email from Bynder
- Occasional inconsistency when uploading assets (doesn't finish) - probably related to the creation of derivatives



## Current process and findings

Image files in the OIPA server are diverse in type, content, size, but mostly tend to be organized on meaningful folders. However, they rarely have any descriptional metadata, and apart from photos from the Photolab, copyright information is often lacking.

The first photos I used for adding a pattern in metadata and testing with Bynder were from the Editorial folder. Caption and title were copied from the ones used in the news piece, together with copyright, and all I had to create was a tagging system. This week in a meeting, rules were set for the tagging system, mentioned in "Metadata fields".

This week I began processing the folder "Scientific Images" to experiment on a harder set of images. Without the direct link to a news piece, and usually lacking information besides the filename, each photo took substantially more time to process than the Editorial folder photos. This process involved using some keyword in the filename, like Briggs, and searching the embl site for a match, allied with the date of the picture. This bottleneck can sometimes make 1 photo take 15 minutes to be found and updated, and when it is present on a press release/news. Those not found are left pending until I ask other people for help, which will be done soon. 

### Findings

- Lack of information on photos, often just folder and file name, makes the update of metadata a very slow process
- News and press releases have been key to have good captions, tags, and copyright information. When unconnected with press and news releases, the caption is often a slightly longer title
- Hard to define what is worth keeping. Small sized images, lower than 1000 px on the larger axis, are usually eliminated, apart from microscopy images when they retain a "good" look. I'm assuming there's a technological limitation for capturing higher pixel count in microscopy photos, will have to check.
  - What is worth keeping?



## Next steps and thoughts

Currently I believe that the next move is to perform an overall assessment of how many images exist in the OIPA server and one final decision process on what is to keep or discard, including those of interest to keep but not to be uploaded to Bynder. This catalog will show how big is the current stock of imagery, how long it might take to include them all into Bynder, and recognize the distribution of image types as evidence to support new image needs/incentives.

Perhaps creating informal use case personas (unsure if that's the word I think it is) is a good method to evaluate actions and results.

 - Suggested action timeline: last week of June (week 26)

### Use cases for uploading/downloading images to Bynder

- Editorial: finding images for articles

- Design: sharing approved/finished images

- Press: create and share press kits

- Scientist: uploading images from experiments

- Scientist: using images for presentations, conferences, etc

- Outreach:

- CCO:

- others...

### Future
When access to Bynder is set to be expanded outside of the Stratcom department, a chapter dedicated to this step will be created and be based on the outcomes of what this report was intended to present.

