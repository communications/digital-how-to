---
title: "Photolab-Design-Digital Workshop"
date: "2018-08-09"
---

## Workshop to define copyright, license, credits, and workflow regarding photographs

### Agenda

1. Quick introduction to Bynder
2. Copyright/License/Credits

    2.1 What is your current procedure?
    
    2.2 Copyright and License
    
    2.3 Credit/Author
3. Workflow: how to make sure that we use Photolabs output+resources best
4. Retouching of images
5. Commissioned images, f.e. core content images to feed Bynder
6. Standard portraits

- Actions to be taken
- Presences

#### 1. Quick introduction to [Bynder](https://resources.embl.org/account/dashboard/)

  - What is Bynder?
      - good way to store content, search metadata, share files 
  - How we use it?
      - mainly as an image repository
  - What goes in?
      - core images, maps, etc

#### 2. Copyright/license/credit

##### 2.1. What is your current procedure?

- Copyright: embl/photographer's name
- License: not filled
- Credit: embl photolab

##### 2.2. Copyright and License

*Copyright © [year] EMBL*

This content is available under a **CC BY-NC** license. This license lets others remix, tweak, and build upon your work non-commercially, and although their new works must also acknowledge you and be non-commercial, they don’t have to license their derivative works on the same terms.

For restricted content (e.g. logo), I think we need more information to support point 2 below.

Legal have set up a call with their opposite team at CERN to look into the issues around the rights to using people’s image, more to follow in due course. In the meantime please use common sense (e.g. assume that we can use photography of all senior EMBL staff, but don’t assume this for guests or students).

- Video might have issues with current license - Claudiu
- Artwork the same - Tabea

**What does it mean?**
It's an official permission or permit to do, use, or own something (as well as the document of that permission or permit). [wikipedia](https://en.wikipedia.org/wiki/License#cite_note-cam-1)

A shorthand definition of a license is "an authorization to use licensed material".

**Example:**

CC BY–NC (Creative Commons – non commercial)

* [Intranet CC info](https://intranet.embl.de/scientific_services/library/news_public/2018/18-6-cc/index.html)
* [About CC licenses](https://creativecommons.org/license/)
* [Choose a CC license](https://creativecommons.org/choose/)

##### 2.3. Credit/Author

The general format should be 'Person/Institution' and that any EMBL photos should be credited as:

**Person/EMBL**

so we don't differentiate things like 'EMBL Rome', 'EMBL-EBI', or 'EMBL Photolab' – it's all just 'EMBL'. 
Ed will add this to the EMBL style guide that we're developing at the moment.

#### 3. Workflow

How to make sure that we use Photolab's output and resources best?

**Ideas:**

- Drop-things-folder on Photolab server
  - Bynder's waiting-room
- Make file naming standard: Year-month-day_who_what_where
  - asking for date not in the beginning - Udo
- Credit standard naming: Person/EMBL
  - "first name" "last surname"/EMBL
- Metadata within a file (description)
- Upload only images in RGB, no CMYK. CMYK will be in the OIPA server - Design Core
  - Photolab delivers eciRGB as the original

#### 4. Retouching of images

- Is Photolab retouching and improving images?
  - Do the photos in RAW, basic adjustments, no major changes
- Can Stratcom retouch and improve images?
- Can Stratcom create new image compositions?
  - Photolab is fine with that
- Should there be any feedback loop?

#### 5. Commissioned images
##### core content images to feed Bynder

- What should be the process for that?
  - Outstations: yearly 1 or 2 photographers can go to an outstation and do it
  - Used to be an Annual Report thing
  - When planning an outing, to contact someone in Stratcom
- Timeline?
  -  Non urgent matters - a request can be sent and Photolap will fit it in their projects
- How many days do we need to book you before the actual event/photo shooting?
  - As early as possible
- Does Photolab have a briefing template in place?
  - Iris has a briefing form for videos
  - Hugo will send us a template
- For photography as well as for videography?
  - yes
- How should the briefing process be set up?

#### 6. Standard portraits

- Are there any standards for this in place already?
  - Biometric and for the webpage
  - Outside the studio to be discussed (non standard)
- Are these images usually not retouched?
  - Simple retouching. Head on white background.
- Is there a process for updating the portrait after several years?
- Do you overview the portraits being taken on other sites?
  - Very dependent on site staff, equipment, and whose responsibility will it be 

**Feedback from EMBL community:**

- Especially portraits from other sites do have a different (lower) quality.
- The face is differently positioned within the imageframe.
- Light setting is sometimes different.

----

### Actions to be taken

- Bynder training for Photolab members (Udo, Marietta, Hugo, new photographer, Claudiu?)
- Share Bynder guidelines with Photolab for feedback
- Review the templates (from Claudiu and Hugo) - come up with a final version
- Define the workflow with photolab for importing images in Bynder (Waiting room, when, who)
- Follow up with legal (Dan) - examples from CERN and ESO
- Send a list of images for commissioning to Photolab
- Google Doc for Core images
- Ask Photolab to provide a shortlist in Bynder's Waiting-room of comissioned photos
- Define on other stations who will be the dedicated photographer (staff or freelancer) and coordinate standard procedures
- List of people to make updated portraits

### Presences:

- Hugo
- Filipe
- Udo
- Tabea
- Claudiu
- Doros
- Spilios

