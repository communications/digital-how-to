+++
title = "Administration"
date = 2019-06-18T14:00:00+02:00
pre = "<b>X. </b>"
chapter = false
weight = 20
+++

This site is created using [Hugo](https://gohugo.io/) a static site generator, and with a slighty changed theme called [Learn](https://learn.netlify.com/en/).

All the content is written in [Markdown](https://www.markdownguide.org/), and sometimes using [Learn's shortcodes](https://learn.netlify.com/en/shortcodes/) for extra functionalities like with notices, attachments, diagrams, etc.

Content files live in the *Content* folder, have an *md* extension, and begin with a YAML/TOML piece of data called frontmatter:

```
+++
title = "Some title"
menuTitle = "Title"
date = 2019-06-18T14:00:00+02:00
weight = 20
+++
```

