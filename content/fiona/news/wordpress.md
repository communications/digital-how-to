+++
title = "Adding an article in Wordpress"
menuTitle = "Wordpress"
date =  2019-06-15T15:00:00+02:00
weight = 5
+++

[Access the News EMBL administration area](https://news.embl.de/wp-login.php?itsec-hb-token=embletc-web)

![What does what](../guide.png?width=20pc "Click to enlarge")


|      |      |
| ---- | ---- |
| Title | Keep it to less than 6 words to avoid text bleed |
| Article intro | This is the post's standfirst/intro paragraph, and is automatically formatted (similar to H2 style). Please ensure copy-pasted text is unformatted and avoid using bold. Note that at the moment, this text does not appear in RSS readers – any crucial information should be paraphrased in the 1st paragraph of the main text |
| Feature post | Tick this box if you want the story to appear 'waterfall' on the news.embl.de homepage. If you don't tick this box, story will appear in 'Latest' list |
| Excerpt | **Max. 100 characters with spaces.** This shows up on homepage in slots with no image, and when the article is included in a list (e.g. in search results; or when you click on the relevant category or tag) |
| Main text | *1<sup>st</sup> paragraph:* repeat any crucial information contained in *Article intro* (for the benefit of RSS users). If the story is going on the embl websites and the screens in HD, this paragraph should ideally be 110 words maximum and stand alone.</br>*Body text:* Paragraph</br>*Subtitles:* Heading 2</br>*Interview questions:* Heading 3</br>*Images, text boxes, etc.:* refer to [this page](https://www.ebi.ac.uk/seqdb/confluence/display/EMBLCOMMS/Visual+features+of+EMBL+News+website+on+Wordpress) |
| Time to read this article | Word count divided by 200 (rounded up) |
| Related links | Click 'add related link' button to insert a link. Note that if you don't fill in the 'link description field', the link will not show up in the published article |
| Article sources | **Only for stories with a published paper**</br>*Source Description:* Lead authors(s), *et al.* Publication, (published online) day month year, DOI (note: if there are joint 1<sup>st</sup> authors, list them. If not, then just the 1<sup>st</sup> author)</br>*Source link URL*: direct link to the DOI URL (Note: for articles not published yet when uploading, if the DOI is already known, the URL is http://dx.doi.org/ and then the DOI)</br>**Note:** at the moment we can only have one source due to formatting issues. If there is more than one reference to be included with a story, put the first one in here and the other(s) in the "Related links" field |
| Description PDF archive link | Only for articles published first elsewhere. Remember to delete automatic text |
| PDF link | Link to pdf where the article was first published |
| Yoast SEO | Check [this page](https://www.ebi.ac.uk/seqdb/confluence/display/EMBLCOMMS/Using+Yoast%3A+Search+Engine+Optimisation+for+your+posts) |
| Author | One-off/multiple authors should be listed as 'Guest author(s)' – add individual names in byline, at start of body text, H3. [example](https://news.embl.de/lab-matters/1703-women-in-science-glass-half-full/) |
| Categories | Deselect 'uncategorised' and select **one** category:</br>Science - for science-related stories</br>Events - stories that relate to courses, conferences and other (public) events</br>Alumni - stories that revolve around EMBL alumni</br>Lab matters - stories about EMBL: governance, life at EMBL, anything that doesn't fit in one of the other categories | 
| Tags | For consistency, click 'Choose from the most used tags' to see if there are already tags in use that fit well with your story |
| Featured image | **620x425 pixels** (if possible)</br>This is the main image at the top of the post. Very long captions may create some formatting issues – check in preview and edit when necessary. Avoid repeating the "Featured image" in the main body of the article.</br>*Spilios on April 16:* Keep the title of the images simple: date-keywords ("180405-pdx-finder") |

{{% notice note %}}
If the title and the excerpt are too long they will overlap in one of the blocks on the news page.
![Example](../Screenshot_2019-07-17_at_09.39.23.png)
{{% /notice %}}


- Preview your story one last time to check everything is OK
- Save your draft (or change status to 'Pending Review' if we decide to use this functionality)
- Log out of Wordpress when finished. This tells Wordpress you're done with this story, and frees it up for others.


{{% notice tip %}}
When copying from a text editor like Microsoft Word, pay attention to the formatting being used. If a reasonable option, copy as plain text and then do the formatting in Wordpress.
{{% /notice %}}


{{% notice note %}}
If the content was originally published somewhere else, you should include this at the bottom of the article:</br>This post was originally published on ```<a href="insert URL" data-href="insert URL" rel="canonical nofollow noopener" target="_blank">EMBL-EBI News</a>```</br>*Oana Stroe: April 05, 2018*
{{% /notice %}}
