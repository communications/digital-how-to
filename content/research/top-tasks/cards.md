+++
title = "Cards"
date =  2018-10-31T14:42:23+01:00
+++

### Cards used in the Top 5 Tasks

| id  |  A  |  B  |  C  |  D  |
|:---:|:---:|:---:|:---:|:---:|
| 1 | Contact of a specific person | Alumni information | HR information | Marketplace |
| 2 | Calendar | Form | Internal events list | Staff association information |
| 3 | Contact of someone in a specific role | Group information | Links to tools | Unit information |
| 4 | Newcomer information | Shuttle timetable | Template | Legal information |
| 5 | Booking a room | Service request (IT) | SAP link | HR how-to |
| 6 | Purchasing information | Webmail link | Scientific service information | WiFi, Eduroam |
| 7 | Service request (instruments) | External events list | Printing | Training information |
| 8 | IT how-to | Remote Access (VPN) | Pay/benefits information | Email settings |
| 9 | PhD information | Health insurance information  | Health forms | Vacation information |
| 10 | Policy document | Email vacation/out-of-office message | Brexit | Leave information (not leaving EMBL) |
| 11 | Conditions of employment | Leaving EMBL | Canteen menu | Diplomatic plates |
| 12 | Timesheets | TSC portal | Intermedex | Kindergarten information |
| 13 | Travel reimbursement form | Kindergarden menu | Other outstations information | Internal news |
| 14 | Living in Heidelberg information | Car insurance information | Library catalogue | Change passwords |
| 15 | Language course reimbursement | Available job positions | Mailing list information | Facility management information |
| 16 | Converis ticket | CMS support | SAP ticket | Catering order form |
