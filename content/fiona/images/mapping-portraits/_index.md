+++
title = "Mapping staff portraits"
menuTitle = "Mapping staff portraits"
date =  2018-08-21T19:54:00+02:00
weight = 15
+++

Staff portrait photos appear in the staff member page on the intranet and on the external website. These portraits are mapped to a staff member via [Image Mapper](https://webda.embl.de/EMBLPersonGroup-ManageImages/).

{{% notice note %}}
Keep an eye for **predoc** portraits when mapping photos to names. If there's a predoc portrait available, save the image to a folder. The filename should have the person's name.</br>
*Visiting predocs are not part of the EMBL Programme - Katie, 14/08/18.*
{{% /notice %}}

### Instructions

The images on the right side box ("New pictures available") should be mapped to the respective persons on the left side box ("All persons at EMBL").

1. To filter the list, enter the name in the search fields on the bottom of the left side box and click Enter or Tab;

	![Point 1 screenshot example](image-mapper-1.jpg?width=15pc "Click to enlarge")

2. To see the job titles click the arrow button on the top right corner of left side box and select it;

	![Point 2 screenshot example](image-mapper-2.png?width=13pc "Click to enlarge")

3. To map images to names, simply drag and drop the image to the respective person.


{{% notice tip %}}
The column labeled "Show image" changes from "No image available" to "Show current image". You can preview the current image by clicking on that button.
{{% /notice %}}

### When you have predocs photos

1. Resize the images to 115x130px;

2. Zip the files and <a href="mailto:predocs@embl.de">email</a> them to Matija and Carolina from the Graduate Office, so that they can add them to the [Predoc Classes pages](https://intranet.embl.de/training/eipp/who/classes/).
