---
title: "Result of interviews"
date: "2018-07-18"
---

## Understanding the use of images in the workflow

Interviews were run to understand how workflows are affected with the current usage of images, what kind of changes are desirable, and any topic image related.

### Social

- Pictures of people, cool and pretty Science images
- Web content focus

### Digital Editor

- Web content focus

### Strategy

- Presentation focus
- PPT and templates in Bynder?

### Webdesign

- Hotlinking for website management?
- Style-lab page

### Outreach

- Press focus (rights, license)
- Science pictures with description
- How often was a photo used?
- Evergreen photos

### Administration

- Report focused
- Publications on Bynder? (Dan)
- Group leader portraits

### Design

- Print focus
- Credits, license
- Coordination with Editorial

### Science in School

- Mostly external sources for images
- External contacts (like press?) 

### Editorial

- Eyecatching photos
- Web focused but may be needed in print
- Coordination with Design and Press
- Direct request of photos from scientists, many times boring or crap
- Pitches often lack a design approach
- No license/credit, not used
- Interested in images with a description (when, what, who, where)
- Press pack

### Press

- Focus: Press releases (always with an image) and Conference presentations (uncommon)
- Coordination with Design
- Source of pics: researcher's cool photos
- Knowing credits and license
- If on Bynder, images have to be checked if they can be shared (according to Dan a notice on the nature of Bynder will be present and who uploads is responsible to see if the pics can be shared)
- Portrait photos from VIP to share with external parties

### Internal

- TBD



## Everyone

- The bigger the better
- Proper description
- License
- Searchable



## Print needs

- Science for Schools
- Administration
- Design
- Editorial

