+++
title = " Updating the EMBL organigramme"
menuTitle = "Organigramme"
date =  2018-08-21T16:50:58+02:00
+++

Once per month we receive an email from Elke Jagomast (Senior Human Resources Officer and Deputy Head of Human Resources) with the request to update the organisation's organigramme. She sends a power point file.

- [Intranet](https://intranet.embl.de/leadership/dg/organigramme/index.html)
- [Current organigramme](https://intranet.embl.de/leadership/dg/organigramme/organigramme-all.pdf)



## Actions

1. Edit the powerpoint file to make sure that the text doesn't flow outside of the boxes - a quick fix only;

2. Save it as a PDF;

3. Follow the link to the [Fiona folder](https://nps2.embl.de:8443/default/NPS/p/b_c8/cb*/EMBL/content/aboutus/01-general_information/01-common/leadership/dg/organigramme*#x);

4. Go to *File* > *Import File*;

5. Select the pdf you created:
   1. option "The current version of the file will be replaced"
   2. click *Import*;

6. Change the "Last update" date on the main text;

7. Release the page.

