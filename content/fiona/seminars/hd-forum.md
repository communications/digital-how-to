+++
title = "Science & Society Heidelberg Forum"
menuTitle = "Science & Society HD Forum"
date =  2019-06-13T14:30:00+02:00
+++

[Science & Society Heidelberg Forum](https://www.embl.de/aboutus/science_society/hd_forum/index.html)

[Science & Society Heidelberg Forum folder in Fiona](https://nps2.embl.de:8443/default/NPS/p/b_10/cb*/EMBL/content/aboutus/03-science_society/01-common/science_society/hd_forum/)

### Creating a speaker page

1. Inside the *upcoming* folder, use *Edit > Copy tree* of an existing folder and paste it in *upcoming* folder.

	{{% notice tip %}}
This procedure copies the folder and any files within the folder. Careful on how deep it goes, or if there are large files involved.
{{%/ notice %}}

2. Edit the image inside the folder:
   - *File > Import file* and replace the photo (115x130px);
   - Rename the file to the speaker's surname;
   - Change the title to the speaker's name.

3. In the folder:
   - Change the name to the speaker's surname;
   - Change the *Title* to the talk's title;
   - Edit the *float column* to reflect the speaker's name. The photo should already be the correct one that you changed before;
   - Edit the main content accordingly.

	{{% notice note %}}
If the abstract or biography is missing, write *To be added* in italics (mind the talk's language) until you're provided with one.
{{%/ notice %}}

### Adding a speaker to the list

1. Copy and paste one of the images below the speaker folder, rename it and replace the image.

2. Change the title to the speaker's name.

3. Adapt accordingly the *text column* field of the image.

4. If the previews are good, release all of the content (folders and images).

### Editing the seminar database

[Seminar Database](https://www-db.embl.de/jss/seminarlist.html?administer=x&administer=x)

- Make sure to be on the correct EMBL location.
- Find the intended talk and click *edit* (right side) to change the contents.
- Change accordingly and confirm that the event shows the information correctly.

{{% notice note %}}
If the abstract or biography is missing, **don't** write *To be added*, just leave it blank. Make sure to update this section when you're provided with one.
{{%/ notice %}}

{{% notice tip %}}
The seminars database updates all information immediately, and you can find the seminars on the EMBL homepage under the news.
{{%/ notice %}}

### Moving to past events

Once the event is over, we must move it to the [Past forums folder](https://nps2.embl.de:8443/default/NPS/p/b_10/cb*/EMBL/content/aboutus/03-science_society/01-common/science_society/hd_forum/past_forums/)

1. Copy and paste the entire contents of the talk (folder and images) to the *past_forums* folder.

2. Disable and release the talk (folder and images) from the *upcoming* folder.

3. In the recently pasted talk files in the *past_forums* folder, adapt the locations of the images to account for the new location (if needed).

4. Set an order for the image (text with image) to be higher than the last one on the [published page](https://www.embl.de/aboutus/science_society/hd_forum/past_forums/index.html).

4. If the previews are good, release all of the content (folders and images).

----

The Science & Society contact person is [Halldór Stefánsson](mailto:halldor.stefansson@embl.de)

