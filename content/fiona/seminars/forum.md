+++
title = "Science & Society Forum"
menuTitle = "Science & Society Forum"
date =  2019-06-13T14:30:00+02:00
+++

[Science & Society Forum](https://www.embl.de/aboutus/science_society/forum/index.html)

[Science & Society Forum folder in Fiona](https://nps2.embl.de:8443/default/NPS/p/b_10/cb*/EMBL/content/aboutus/03-science_society/01-common/science_society/forum/)

### Creating a speaker page

1. Inside the *forums_20xx* folder, use *Edit > Copy tree* of an existing folder and paste it in *forums_20xx* folder.

	{{% notice tip %}}
This procedure copies the folder and any files within the folder. Careful on how deep it goes, or if there are large files involved.
{{%/ notice %}}

2. Edit the image inside the folder:
   - *File > Import file* and replace the photo (155x175px);
   - Rename the file to the speaker's surname;
   - Change the title to be *name, workplace*.

3. In the folder:
   - Change the name to the correct month-day of the talk;
   - Edit the *float column* to reflect the speaker's name and workplace. The photo should already be the correct one that you changed before;
   - Edit the main content accordingly.

	{{% notice note %}}
If the abstract or biography is missing, write *To be added* in italics until you're provided with one.
{{%/ notice %}}

### Adding a speaker to the list

1. Open the *Main content* of the *forums_20xx* folder, copy an existing item and change accordingly.

2. Copy and paste one of the images below the *forums_20xx* folders, rename it and replace the image.

3. Depending on when is the talk, find a suitable order for this speaker to show up in the list.

4. Copy what you added in the *Main content* of the *forums_20xx* folder and add the same information in the *text column* field of the image.

5. Set a *Valid until* date to make Fiona hide the image after the talk is finished.

6. If the previews are good, release all of the content (folders and images).

### Editing the seminar database

[Seminar Database](https://www-db.embl.de/jss/seminarlist.html?administer=x&administer=x)

- Make sure to be on the correct EMBL location.
- Find the intended talk and click *edit* (right side) to change the contents.
- Change accordingly and confirm that the event shows the information correctly.

{{% notice note %}}
If the abstract or biography is missing, **don't** write *To be added*, just leave it blank. Make sure to update this section when you're provided with one.
{{%/ notice %}}

{{% notice tip %}}
The seminars database updates all information immediately, and you can find the seminars on the EMBL homepage under the news.
{{%/ notice %}}

### Adding content to the server

1. Copy and paste the content (image, abstract, bio) to the [OIPA server](smb://oipa.embl.de/oipa/_DESIGN/EMBL_ScienceAndSociety) in the relevant folder
2. Let [Tabea](mailto:tabea.rauscher@embl.de) and [Verena](mailto:verena.viarisio@embl.de) know

----

The Science & Society contact person is [Halldór Stefánsson](mailto:halldor.stefansson@embl.de)

