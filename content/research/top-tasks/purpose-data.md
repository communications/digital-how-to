+++
title = "Data by purpose"
date =  2018-11-12T08:42:23+01:00
+++

### Purpose on card choices
#### Question 6b - Which ones of the top 5 are directly work related?
##### Data added from user 1 to user 10

| Work | Personal | Card                                  |
|:----:|:--------:| ------------------------------------- |
|**x** |          | Contact of someone in a specific role |
|**x** |          | Booking a room                        |
|**x** |**x**     | HR information                        |
|**x** |**x**     | Internal news                         |
|**x** |          | Group information                     |
|**x** |          | Service request (IT)                  |
|**x** |**x**     | Internal events list                  |
|**x** |          | Contact of a specific person          |
|**x** |          | Links to tools                        |
|**x** |          | Library catalogue                     |
|**x** |          | IT how-to                             |
|**x** |**x**     | Kindergarten information              |
|      |**x**     | Staff association information         |
|**x** |**x**     | Unit information                      |
|**x** |          | Policy document                       |
|**x** |          | Mailing list information              |
|      |**x**     | Canteen menu                          |
|**x** |          | Calendar                              |
|      |**x**     | Intermedex                            |
|**x** |          | Purchasing information                |
|      |**x**     | Training information                  |
|**x** |          | Converis ticket                       |
|**x** |**x**     | SAP ticket                            |
|**x** |          | CMS support                           |
|**x** |          | Catering order form                   |
|      |**x**     | PhD information                       |
|**x** |          | Facility management information       |
|**x** |**x**     | Email vacation/out-of-office message  |
|**x** |          | Internal events                       |
|**x** |          | Template                              |

#### User data from Top Tasks

| Order | user 1 | user 2 | user 3 | user 4 | user 5 |
|:-----:|:------:|:------:|:------:|:------:|:------:|
| **1** | Contact of someone in a specific role | Internal events list | Group information | Contact of a specific person	 | Calendar |
| **2** | Unit information | Contact of a specific person | Contact of someone in a specific role | Group information | Contact of someone in a specific role |
| **3** | Links to tools | Booking a room | IT how-to | HR information | Training information |
| **4** | Booking a room | Library catalogue | Links to tools | Internal events list | Purchasing information |
| **5** | HR information | Contact of someone in a specific role | Kindergarten information | Policy document | Internal news |
| **6** | Staff association information | Service request (IT) | Internal news | Mailing list information | Intermedex |
| **7** | Service request (IT) |  | HR information | Booking a room |  |  |
| **8** |  |  |  | Canteen menu |  |

----

| Order | user 6 | user 7 | user 8 | user 9 | user 10 |
|:-----:|:------:|:------:|:------:|:------:|:-------:|
| **1** | Contact of a specific person | Booking a room | Booking a room | Contact of a specific person | Contact of a specific person |
| **2** | Service request (IT) | Canteen menu | Contact of a specific person | Booking a room | Group information |
| **3** | Converis ticket | Contact of a specific person | Catering order form | Facility management information | Booking a room |
| **4** | SAP ticket | Intermedex | Contact of someone in a specific role | Group information | HR information |
| **5** | CMS support | PhD information | Mailing list information | Catering order form | Template |
| **6** | | | | Email vacation/out-of-office message | Policy document |
| **7** | | | | | Internal Events |

