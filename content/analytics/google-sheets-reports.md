---
title: "Reports with Google Sheets"
date: 2019-03-28T14:23:28+01:00
---

[EMBL Performance Dashboards](https://dev.beta.embl.org/performance-dashboards/)

[EMBL Performance Dashboards repository](https://gitlab.ebi.ac.uk/emblorg/embl-performance-dashboards)

## Why?

Using analytics is paramount to understanding the role of digital services and users interests. What works, what is underserved, what is engaging, and how can improvements be made, needs the backing of data and evaluation methods that lead to actions.

## In the Report Configuration sheet

#### Using the Report maker

{{<mermaid align="center">}}
graph LR;
    subgraph Top Menu of Google Sheets
    A(Add-ons) --- B(Google Analytics)
    B --- C(Create new report)
    end
    C --- D(1: Add name)
    subgraph 
    D --- E(2: Select a view)
    E --- F(Metrics)
    subgraph 3: Choose configuration options
    F -.- H(Dimensions)
    end
    end
    F --- J[Create Report]
    H --- J[Create Report]
{{< /mermaid >}}

#### Creating a report on the spreadsheet

You can create a report directly on the spreadsheet, which with practice makes the creation of reports faster, more customisable, and easy to copy to another file.

Google has documentation for defining [metrics](#) and [dimensions](#), and overall information on [customising reports](#).

## Spreadsheets and account

The account is embl-dashboards@ebi.ac.uk and is managed by Ken Hawkins. A mailinglist was also created to receive notifications of errors or GA limitations.

Once logged in, the spreadsheets can be seen on the [Drive folder](https://docs.google.com/spreadsheets/u/1/?pli=1&tgif=d)

{{% notice warning %}}
Bear in mind the limitations in the [Data Workflow page](https://grp-stratcom.embl-community.io/digital-how-to/analytics/data-workflow/)
{{% /notice %}}

