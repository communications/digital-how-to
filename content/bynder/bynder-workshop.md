---
title: "Kick-off Bynder Workshop"
date: "2018-06-06"
---

## Workshop to test Bynder as EMBL's DAM tool

### Goal:

The goal of this workshop is to agree on some minimum requirements (on customisation and workflows) that will allow us:

- To start using Bynder within our team.
- To start the process of populating Bynder.
- To decide on the next steps (rough timelines as well).
- And to test more things...

1. Final set up - customisation
   - Define metaproperties/taxonomy.
     - Additional asset types?
     - More image categories?
   - Go through the image derivative sizes. Do we need different ones?

2. Create "Rules of use" / Guidelines
   - File names format (e.g. YYMMDD-keyword?).
   - Asset metadata (what kind of information should be added to the description field).
     - Date?
     - Credits?
     - Caption (if it’s in the news)?
     - Link (if it’s in the news)?
     - Copyrights?
   - Define image specifications.
   - Define image collections - how do we use this feature?
   - Cropping of images and saved as additional derivatives.
   - How do we deal with image copyrights?
   - Tagging.

3. Workflows
   - Approval process.
     - Who?
     - When?
   - Access rights for team members.
   - Start populating Bynder with images.
     - Who?
     - When?
     - What?

4. Roadmap

   - Next steps and rough timelines.

### Participants:

- Mark Boulton
- Tabea Miriam Rauscher
- Cian O´Luanaigh
- Laura Howes
- Edward Dadswell
- Spencer Phillips
- Spilios Vasilopoulos
- Filipe Dias.

[Bynder Roadmap - Google Docs](https://docs.google.com/spreadsheets/d/1i67HUPLg9V1I-kOIpOuugtHZDcBTAyvwx8G-aGdirZY/edit?ts=5b165cf8#gid=0)
